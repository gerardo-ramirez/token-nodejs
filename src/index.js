const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();
const root = require('./routes/route');
//setting
app.set('port', process.env.PORT || 8080);

//routes
app.use(root);

//LISTEN
app.listen(app.get('port'), ()=>{
  console.log('conection')
});
