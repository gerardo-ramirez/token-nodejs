const express = require('express');
const root = express.Router();
//usamos token:
const jwt = require('jsonwebtoken');


root.get('/', (req,res)=>{
  res.json({text: 'hola mundo'})

});
root.post('/api/login',(req,res)=>{
  const user = {id: 3};
  //generamos el token para ese usuario:
  //recibe el usuario y una clave, variable de entorno, para cifrar y decifrar la info.
  const token = jwt.sign({user},'my_secret_key' );
//my_secret_key : genera el token.
  res.json({token});


});
//para crear la ruta protegida debemos crear una funcion para pasar el token
//esta funcion seria un middleware.
const ensureToken = (req,res,next) => {
  //revisamos que en la cabecera  este la autorizacion del token.
  const bearerHeader = req.headers['authorization'];
  console.log(bearerHeader);//que valor viene
  //hacemos condicional:

  if(typeof bearerHeader !== 'undefined'){
    //partimos el dato  para que no me devuelva la cabecera y el token.
    //solo queremos el token.

    const bearer = bearerHeader.split(' ');//lo parte a partir del espacio en
    //blanco y devuelve un array.
    const bearerToken = bearer[1];
    req.token = bearerToken; // le paso el token a la peticion.
    //luego le premito continuar.
    next();
  } else {
    res.sendStatus(403);
  }
};
//creamos ruta protegida.

root.get('/api/protected',ensureToken,(req,res)=>{
  //validamos el token con metodos de jwt.
  jwt.verify(req.token, 'my_secret_key', (err,data)=>{
    if(err) {
      res.sendStatus(403)
    } else {

        res.json({
          text:'protected',
          data
        });

    }
  });


});
module.exports = root ;
